using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAim : MonoBehaviour
{
    [SerializeField] GameObject rightPerson;
    [SerializeField] GameObject leftPersion;

    private void Update()
    {
        if (Vector3.Distance(rightPerson.transform.position, MapManager.ins.milestone.transform.position) < Vector3.Distance(leftPersion.transform.position, MapManager.ins.milestone.transform.position))
        {
            transform.position = rightPerson.transform.position;
        }
        else
            transform.position = leftPersion.transform.position;

    }
}
