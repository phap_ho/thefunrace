using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


 
public class PlayerControl : MonoBehaviour
{
    private Animator animator;
    [SerializeField] FreeLookCam freeLookCam;

    private void Awake()
    {
        animator = GetComponent<Animator>();

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Contains("Obstacles"))
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Death"))
                animator.SetTrigger("Death");
        }

        if (other.tag.Contains("TurnRight"))
        {
            Destroy(other.gameObject);
            MapManager.ins.milestone = other.GetComponentInParent<BlockBase>();
            switch (MapManager.ins.currentDirection)
            {
                case (CurrentDirection.Forward):
                    freeLookCam.Turn(90);
                    MapManager.ins.currentDirection = CurrentDirection.Right;
                    break;
                case (CurrentDirection.Right):
                    freeLookCam.Turn(180);
                    MapManager.ins.currentDirection = CurrentDirection.Back;
                    break;
                case (CurrentDirection.Left):
                    freeLookCam.Turn(0);
                    MapManager.ins.currentDirection = CurrentDirection.Forward;
                    break;
            }
        }
        if (other.tag.Contains("TurnLeft"))
        {
            Destroy(other.gameObject);
            MapManager.ins.milestone = other.GetComponentInParent<BlockBase>();
            switch (MapManager.ins.currentDirection)
            {
                case (CurrentDirection.Right):
                    freeLookCam.Turn(0);
                    MapManager.ins.currentDirection = CurrentDirection.Forward;
                    break;
                case (CurrentDirection.Forward):
                    freeLookCam.Turn(-90);
                    MapManager.ins.currentDirection = CurrentDirection.Left;
                    break;
            }
           
        }
    }
}