using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CurrentDirection
{
    Forward,
    Left,
    Right,
    Back
}

public class MapManager : MonoBehaviour
{
    public Transform reference;
    public List<BlockBase> blockBases;
    public static MapManager ins;

    public CurrentDirection currentDirection;
    public BlockBase milestone;
    private void Awake()
    {
        ins = this;
        //------------------------------------------------------------------------------------//
        currentDirection = CurrentDirection.Forward;
        for (int i = 1; i < 5; i++)
        {
            BlockBase block = Instantiate(blockBases[0], reference.transform.position, Quaternion.identity);
            reference = block.reference;
        }

        for (int i = 1; i <= 5; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 7)], reference.transform.position, Quaternion.identity);
            reference = block.reference;
        }
        //--------------------------------------------------------------------------//
        BlockBase blockRight = Instantiate(blockBases[13], reference.transform.position, Quaternion.identity);
        reference = blockRight.reference;

        for (int i = 1; i <= 5; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 12)], reference.transform.position, Quaternion.Euler(0, 90, 0));
            reference = block.reference;
        }
        //------------------------------------------------------------------------------------//
        BlockBase blockLeft = Instantiate(blockBases[14], reference.transform.position, Quaternion.Euler(0, 90, 0));
        reference = blockLeft.reference;

        for (int i = 1; i <= 5; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 12)], reference.transform.position, Quaternion.identity);
            reference = block.reference;
        }
        //---------------------------------------------------------------------------//
        BlockBase blockRight1 = Instantiate(blockBases[13], reference.transform.position, Quaternion.identity);
        reference = blockRight1.reference;

        for (int i = 1; i <= 5; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 12)], reference.transform.position, Quaternion.Euler(0, 90, 0));
            reference = block.reference;
        }
        //----------------------------------------------------------------------------------//
        BlockBase blockLeft1 = Instantiate(blockBases[14], reference.transform.position, Quaternion.Euler(0, 90, 0));
        reference = blockLeft1.reference;

        for (int i = 1; i <= 20; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 12)], reference.transform.position, Quaternion.identity);
            reference = block.reference;
        }
        //--------------------------------------------------------------------------------//
        BlockBase blockLeft2 = Instantiate(blockBases[14], reference.transform.position, Quaternion.identity);
        reference = blockLeft2.reference;

        for (int i = 1; i <= 20; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 12)], reference.transform.position, Quaternion.Euler(0,-90,0));
            reference = block.reference;
        }
        //----------------------------------------------------------------------------------//
        BlockBase blockRight2 = Instantiate(blockBases[13], reference.transform.position, Quaternion.Euler(0, -90, 0));
        reference = blockRight2.reference;

        for (int i = 1; i <= 5; i++)
        {
            BlockBase block = Instantiate(blockBases[Random.Range(0, 12)], reference.transform.position, Quaternion.identity);
            //block.transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
            reference = block.reference;
        }
        //---------------------------------------------------------------------------------------//
    }
}
