using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReplayGame : MonoBehaviour
{

    public List<GameObject> characters;
   public void ReplayGameButton()
    {
        SceneManager.LoadScene(0);
    }
    private void Update()
    {
        foreach(GameObject character in characters)
        {
            if(character.transform.position.y < -3)
            {
                ReplayGameButton();
            }
        }
    }
}
